package wpm.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javafx.scene.control.Button;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.web.WebEngine;
import properties_manager.PropertiesManager;
import saf.controller.AppFileController;
import saf.ui.AppMessageDialogSingleton;
import saf.ui.AppYesNoCancelDialogSingleton;
import static wpm.PropertyType.ADD_ELEMENT_ERROR_MESSAGE;
import static wpm.PropertyType.ADD_ELEMENT_ERROR_TITLE;
import static wpm.PropertyType.ATTRIBUTE_UPDATE_ERROR_MESSAGE;
import static wpm.PropertyType.ATTRIBUTE_UPDATE_ERROR_TITLE;
import static wpm.PropertyType.CSS_EXPORT_ERROR_MESSAGE;
import static wpm.PropertyType.CSS_EXPORT_ERROR_TITLE;
import wpm.WebPageMaker;
import wpm.data.DataManager;
import wpm.data.HTMLTagPrototype;
import wpm.file.FileManager;
import static wpm.file.FileManager.TEMP_CSS_PATH;
import static wpm.file.FileManager.TEMP_PAGE;
import wpm.gui.Workspace;

/**
 * This class provides event programmed responses to workspace interactions for
 * this application for things like adding elements, removing elements, and
 * editing them.
 *
 * @author Richard McKenna
 * @author Ziling Zhou
 * @version 1.0
 */
public class PageEditController {

    // HERE'S THE FULL APP, WHICH GIVES US ACCESS TO OTHER STUFF
    WebPageMaker app;

    // WE USE THIS TO MAKE SURE OUR PROGRAMMED UPDATES OF UI
    // VALUES DON'T THEMSELVES TRIGGER EVENTS
    private boolean enabled;

    /**
     * Constructor for initializing this object, it will keep the app for later.
     *
     * @param initApp The JavaFX application this controller is associated with.
     */
    public PageEditController(WebPageMaker initApp) {
	// KEEP IT FOR LATER
	app = initApp;
    }

    /**
     * This mutator method lets us enable or disable this controller.
     *
     * @param enableSetting If false, this controller will not respond to
     * workspace editing. If true, it will.
     */
    public void enable(boolean enableSetting) {
	enabled = enableSetting;
    }

    /**
     * This function responds live to the user typing changes into a text field
     * for updating element attributes. It will respond by updating the
     * appropriate data and then forcing an update of the temp site and its
     * display.
     *
     * @param selectedTag The element in the DOM (our tree) that's currently
     * selected and therefore is currently having its attribute updated.
     *
     * @param attributeName The name of the attribute for the element that is
     * currently being updated.
     *
     * @param attributeValue The new value for the attribute that is being
     * updated.
     */
    public void handleAttributeUpdate(HTMLTagPrototype selectedTag, String attributeName, String attributeValue) {
	if (enabled) {
	    try {
		// FIRST UPDATE THE ELEMENT'S DATA
		selectedTag.addAttribute(attributeName, attributeValue);

		// THEN FORCE THE CHANGES TO THE TEMP HTML PAGE
		FileManager fileManager = (FileManager) app.getFileComponent();
		fileManager.exportData(app.getDataComponent(), TEMP_PAGE);
                
                //file has been change
                
                
		// AND FINALLY UPDATE THE WEB PAGE DISPLAY USING THE NEW VALUES
		Workspace workspace = (Workspace) app.getWorkspaceComponent();
		workspace.getGui().updateToolbarControls(false);
                workspace.getHTMLEngine().reload();
                workspace.loadTempPage();
	    } catch (IOException ioe) {
		// AN ERROR HAPPENED WRITING TO THE TEMP FILE, NOTIFY THE USER
		PropertiesManager props = PropertiesManager.getPropertiesManager();
		AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
		dialog.show(props.getProperty(ATTRIBUTE_UPDATE_ERROR_TITLE), props.getProperty(ATTRIBUTE_UPDATE_ERROR_MESSAGE));
	    }
	}
    }
    
    /**
     * This function responds to when the user tries to remove an element from the
     * tree being edited.
     *
     */
    public void handleRemoveElementRequest(){
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
            
        // GET THE TREE TO SEE WHICH NODE IS CURRENTLY SELECTED
	    TreeView tree = workspace.getHTMLTree();
            if(tree.getSelectionModel().isEmpty()){
                AppMessageDialogSingleton message = AppMessageDialogSingleton.getSingleton();
                message.show("No selected Tag","You must a select a tag to begin editing works");
                return;
            }    
	    TreeItem selectedItem = (TreeItem) tree.getSelectionModel().getSelectedItem();
	//    HTMLTagPrototype selectedTag = (HTMLTagPrototype) selectedItem.getValue();
            AppYesNoCancelDialogSingleton yesNoDialog = AppYesNoCancelDialogSingleton.getSingleton();
     
            yesNoDialog.show("Removing a Tag","Do you want to remove: " + selectedItem.toString());
            String str = yesNoDialog.getSelection();
            if(str.equals("Yes")){
                selectedItem.getParent().getChildren().remove(selectedItem);
                workspace.getGui().updateToolbarControls(false);
            }
            try {   
		FileManager fileManager = (FileManager) app.getFileComponent();
		fileManager.exportData(app.getDataComponent(), TEMP_PAGE);
                WebEngine htmlEngine = workspace.getHTMLEngine();
		htmlEngine.reload();
                workspace.loadTempPage();
            } catch (IOException ioe) {
		// AN ERROR HAPPENED WRITING TO THE TEMP FILE, NOTIFY THE USER
		PropertiesManager props = PropertiesManager.getPropertiesManager();
		AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
		dialog.show("Deleting Error","Deleting Error");
	    }
    }
    
    /**
     * This function disable buttons that should not be clicked when user select certain Tags
     * or when user wants to save an unchanged file
     */
    public void foolProofDesign(){
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        DataManager dataManager = (DataManager) app.getDataComponent();
	    TreeView tree = workspace.getHTMLTree();
	    TreeItem selectedItem = (TreeItem) tree.getSelectionModel().getSelectedItem();
	    HTMLTagPrototype selectedTag = (HTMLTagPrototype) selectedItem.getValue();
            String tagName = selectedTag.getTagName(); 
         
            //disable all the html tag button first then decide witch to enable
            for(Button btn: workspace.getTagButton()){
                btn.setDisable(true);
            }
            dataManager.getRemoveBtn().setDisable(false);
            //get all the tag we havee
            for(HTMLTagPrototype tag: dataManager.getTags()){
                //get all the legal parent for each tag
                for(String legalParent: tag.getLegalParents()){
                    //if one of the legal parent is the currently selected treeItem, enable that tag button
                    if(legalParent.equals(tagName)){ 
                        for(Button btn: workspace.getTagButton())
                            if(btn.getText().equals(tag.getTagName())){
                                btn.setDisable(false);
                            }
                        //once we find the action is appropriate we stop continue finding
                        break;
                    }                                      
                }
            }
            //basic node should not be deleted
            if(tagName.equals("html")|| tagName.equals("body") || tagName.equals("link")
                    || tagName.equals("head") || tagName.equals("title")){
                dataManager.getRemoveBtn().setDisable(true);
            }
    }
    
    /**
     * This function responds to when the user tries to add an element to the
     * tree being edited.
     *
     * @param element The element to add to the tree.
     */
    public void handleAddElementRequest(HTMLTagPrototype element,Button clickedBtn) {
	if (enabled) {            
            Workspace workspace = (Workspace) app.getWorkspaceComponent();
            DataManager dataManager = (DataManager) app.getDataComponent();
            
	    // GET THE TREE TO SEE WHICH NODE IS CURRENTLY SELECTED
	   TreeView tree = workspace.getHTMLTree();
           if(tree.getSelectionModel().isEmpty()){
                AppMessageDialogSingleton message = AppMessageDialogSingleton.getSingleton();
                message.show("No selected Tag","You must a select a tag to begin editing works");
                return;
            }    
            TreeItem selectedItem = (TreeItem) tree.getSelectionModel().getSelectedItem();
	    HTMLTagPrototype selectedTag = (HTMLTagPrototype) selectedItem.getValue();

	    HTMLTagPrototype newTag = element.clone();
	    TreeItem newNode = new TreeItem(newTag);

	    // ADD THE NEW NODE
	    selectedItem.getChildren().add(newNode);

	    // SELECT THE NEW NODE
	    tree.getSelectionModel().select(newNode);
	    selectedItem.setExpanded(true);
            
            //file has been change
            workspace.getGui().updateToolbarControls(false);
	    
            // FORCE A RELOAD OF TAG EDITOR
	    workspace.reloadWorkspace();

	    try {   
		FileManager fileManager = (FileManager) app.getFileComponent();
		fileManager.exportData(app.getDataComponent(), TEMP_PAGE);
                WebEngine htmlEngine = workspace.getHTMLEngine();
		htmlEngine.reload();
                workspace.loadTempPage();
            } catch (IOException ioe) {
		// AN ERROR HAPPENED WRITING TO THE TEMP FILE, NOTIFY THE USER
		PropertiesManager props = PropertiesManager.getPropertiesManager();
		AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
		dialog.show(props.getProperty(ADD_ELEMENT_ERROR_TITLE), props.getProperty(ADD_ELEMENT_ERROR_MESSAGE));
	    }
     
            
        }
    }

    /**
     * This function provides a response to when the user changes the CSS
     * content. It responds but updating the data manager with the new CSS text,
     * and by exporting the CSS to the temp css file.
     *
     * @param cssContent The css content.
     *
     */
    public void handleCSSEditing(String cssContent) {
	if (enabled) {
	    try {
		// MAKE SURE THE DATA MANAGER GETS THE CSS TEXT
		DataManager dataManager = (DataManager) app.getDataComponent();
		dataManager.setCSSText(cssContent);

		// WRITE OUT THE TEXT TO THE CSS FILE
		FileManager fileManager = (FileManager) app.getFileComponent();
		fileManager.exportCSS(cssContent, TEMP_CSS_PATH);

		// REFRESH THE HTML VIEW VIA THE ENGINE
		Workspace workspace = (Workspace) app.getWorkspaceComponent();
		WebEngine htmlEngine = workspace.getHTMLEngine();
                
                //file need updata
                workspace.getGui().updateToolbarControls(false);
                
		htmlEngine.reload();
                workspace.loadTempPage();
	    } catch (IOException ioe) {
		AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
		PropertiesManager props = PropertiesManager.getPropertiesManager();
		dialog.show(props.getProperty(CSS_EXPORT_ERROR_TITLE), props.getProperty(CSS_EXPORT_ERROR_MESSAGE));
	    }
	}
    }
}
